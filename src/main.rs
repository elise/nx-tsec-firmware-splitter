use std::{fmt::Display, io::Cursor, ops::Range, path::PathBuf, process::exit};

use binrw::{BinRead, BinReaderExt};
use clap::Parser;
use thiserror::private::PathAsDisplay;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("Usage: firmware-splitter <firmware_path> [out_path]")]
    Args,
    #[error("Path not found: {0}")]
    PathNotFound(PathBuf),
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error("Unable to find key table in provided firmware")]
    UnableToFindTable,
}

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]

pub struct Args {
    /// Path to either a TSEC FW blob or a PKG1 dump
    pub firmware_path: PathBuf,
    /// Only extract the whole TSEC firmware blob from Package1
    #[clap(short = 'e', long = "extract")]
    pub extract_only: bool,
    /// Optional directory to split into
    pub out_directory: Option<PathBuf>,
}

#[derive(Debug, BinRead)]
pub struct KeyTable {
    pub debug_key: [u8; 0x10],
    pub boot_hash: [u8; 0x10],
    pub keygen_ldr_hash: [u8; 0x10],
    pub keygen_hash: [u8; 0x10],
    pub keygen_iv: [u8; 0x10],
    pub hovi_eks_seed: [u8; 0x10],
    pub hovi_common_seed: [u8; 0x10],
    pub boot_size: u32,
    pub keygen_ldr_size: u32,
    pub keygen_size: u32,
    pub secure_boot_ldr_size: u32,
    pub secure_boot_size: u32,
}

impl Display for KeyTable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Debug Key: {:02X?}\nBoot Hash: {:02X?}\nKeygen Hash: {:02X?}\nKeygen IV: {:02X?}\nHOVI EKS Seed: {:02X?}\nHOVI Common Seed: {:02X?}\nBoot Size: {:#X}\nKeygen Ldr Size: {:#X}\nKeygen Size: {:#X}\nSecure Boot Ldr Size: {:#X}\nSecure Boot Size: {:#X}", self.debug_key, self.boot_hash, self.keygen_hash, self.keygen_iv, self.hovi_eks_seed, self.hovi_eks_seed, self.boot_size, self.keygen_ldr_size, self.keygen_size, self.secure_boot_ldr_size, self.secure_boot_size))
    }
}

fn write_blobs(
    mut out_directory_path: PathBuf,
    tsec_fw_base_offset: usize,
    firmware: &[u8],
    blobs: &[(&'static str, Range<usize>)],
) -> Result<(), Error> {
    for (name, range) in blobs {
        let adjusted_range = (range.start - tsec_fw_base_offset)..(range.end - tsec_fw_base_offset);
        out_directory_path.push(format!(
            "{:#06X}_{:#06X}_{}.bin",
            adjusted_range.start, adjusted_range.end, name
        ));
        std::fs::write(
            out_directory_path.as_path(),
            &firmware[range.start..range.end],
        )?;
        println!("Written {} ({:#X?})", name, adjusted_range);
        out_directory_path.pop();
    }

    Ok(())
}

fn actual_main() -> Result<(), Error> {
    let args = Args::parse();

    if !args.firmware_path.exists() || !args.firmware_path.is_file() {
        return Err(Error::PathNotFound(args.firmware_path));
    }

    let firmware = std::fs::read(args.firmware_path.as_path()).map_err(Error::Io)?;

    let bmb = boyer_moore_magiclen::BMByte::from("HOVI_COMMON").unwrap();
    let table_offset = bmb
        .find_first_in(&firmware)
        .ok_or(Error::UnableToFindTable)?
        - 0x60;

    let mut cursor = Cursor::new(&firmware);
    cursor.set_position(table_offset as u64);
    let table: KeyTable = cursor.read_le().expect("Invalid Metatada Table");

    println!("Found firmware table:\n{}\n", table);

    let boot_range = (table_offset - table.boot_size as usize)..table_offset;
    let keygen_ldr_range =
        (table_offset + 0x100)..(table_offset + 0x100 + table.keygen_ldr_size as usize);
    let keygen_range = keygen_ldr_range.end..(keygen_ldr_range.end + table.keygen_size as usize);

    let mut out_directory_path = args.out_directory.unwrap_or_else(|| PathBuf::from("./"));
    std::fs::create_dir_all(out_directory_path.as_path())?;

    let mut blobs = vec![
        ("boot", boot_range),
        ("keygen_ldr", keygen_ldr_range),
        ("keygen_encrypted", keygen_range.clone()),
    ];

    if table.secure_boot_ldr_size != 0 && table.secure_boot_size != 0 {
        println!("Found secure_boot blob, TSEC FW is from version 6.2.0 or above");

        let secure_boot_ldr_range =
            (keygen_range.end)..(keygen_range.end + table.secure_boot_ldr_size as usize);
        let secure_boot_range = (secure_boot_ldr_range.end)
            ..(secure_boot_ldr_range.end + table.secure_boot_size as usize);

        blobs.push(("secure_boot_ldr", secure_boot_ldr_range));
        blobs.push(("secure_boot_encrypted", secure_boot_range));
    }

    if args.extract_only {
        out_directory_path.push("tsec-fw.bin");
        std::fs::write(
            out_directory_path.as_path(),
            &firmware[blobs[0].1.start..blobs.last().unwrap().1.end],
        )?;
        println!("Written {}", out_directory_path.as_display());
    } else {
        write_blobs(
            out_directory_path,
            table_offset - table.boot_size as usize,
            &firmware,
            &blobs,
        )?;
    }

    Ok(())
}

fn main() {
    if let Err(why) = actual_main() {
        eprintln!("{}", why);
        exit(1);
    }
}
