# NX TSEC Firmware Splitter

A tool to extract TSEC FW stages from a Package1 dump or NX TSEC firmware blob.

## Installation

### Prerequesites

* [Rust](https://rustup.rs)

```sh
cargo install --git https://gitlab.com/elise/nx-tsec-firmware-splitter.git
```

## Usage

```sh
nx-tsec-firmware-splitter <path_to_pkg1_or_tsec_fw> [out_directory]
```

## Credits

* Contributors to the Switchbrew Wiki
